/*
 * Author:  Dustin Haring
 * Company: DE Design Works
 * Date:    May 21, 2018
 * Comment: This file contains the declarations and definitions for the state machine class
 * 
 * Modified By: n/a
 * Date:        n/a
 * Comment:     n/a
 */

#ifndef TESTCLASS_H
#define TESTCLASS_H

#include "stateMachine.cpp" //because of templates, you must include the cpp file and not the header file
#include <stdio.h>
#include <thread>
#include <unistd.h>

enum STATES
{
    STATE_A = 0,
    STATE_B,
    MAX_STATES
};

//state independant. restarts state machine
enum EXTERNAL_EVENTS
{
    EEVENT_1 = 0,
    EEVENT_2,
    EEVENT_TIMER_EXPIRED,
    MAX_EEVENTS
};

//state dependant. queues event for when state machine is running
enum INTERNAL_EVENTS
{
    IEVENT_1 = 0,
    IEVENT_2,
    MAX_IEVENTS
};

//this class represents an example implementation
class TestClass : public StateMachine<void (TestClass::*)(EventData *), TestClass>
{
  public:
    TestClass();
    ~TestClass();

    void initializeStateMachineMap();
    void nullFunction(EventData *EData); //nullFunction. all possible state/event combinations are initialized to this because it was passed in the StateMachine contructor
    
    void start();
    void timer(unsigned int, int);//, TestClass* parent);
    
    void doSomething_A1(EventData *EData){printf("doSomething from state A because of event 1\n");}
    void doSomething_A2(EventData *EData){printf("doSomething from state A because of event 2\n");}
    void doSomething_B1(EventData *EData){printf("doSomething from state B because of event 1\n");}
    void doSomething_B2(EventData *EData){printf("doSomething from state B because of event 2\n");}

    void externalEvent_1(EventData *EData) {raiseIEvent(IEVENT_1);}
    void externalEvent_2(EventData *EData) {raiseIEvent(IEVENT_2);}

  private:
    /* data */
};

TestClass::TestClass() : StateMachine(this, MAX_STATES, MAX_IEVENTS, MAX_EEVENTS, &TestClass::nullFunction)
{
    initializeStateMachineMap();
}

void TestClass::initializeStateMachineMap()
{
    //list all state/event/function maps here
    //current state   //internal event  //new state     //function to call
 // map(  STATE_A,        IEVENT_1,        STATE_A,        &TestClass::doSomething_A1  );
    map(  STATE_A,        IEVENT_1,                        &TestClass::doSomething_A1  );//this line means the new state is the same as the current state. equivalent to commented line above
    map(  STATE_A,        IEVENT_2,        STATE_B,        &TestClass::doSomething_A2  );
    map(  STATE_B,        IEVENT_1,        STATE_B,        &TestClass::doSomething_B1  );
    map(  STATE_B,        IEVENT_2,        STATE_A,        &TestClass::doSomething_B2  );

    //External Event        //funct to call
    map(  EEVENT_1,                 &TestClass::externalEvent_1  );
    map(  EEVENT_2,                 &TestClass::externalEvent_2  );
    map(  EEVENT_TIMER_EXPIRED,     &TestClass::externalEvent_2  );

}

void TestClass::start()
{
    raiseEEvent(EEVENT_1);//reverse these lines for a separate path through the states.
    raiseEEvent(EEVENT_2);//reverse these lines for a separate path through the states.

    //test the multithreaded access
    std::thread timerThread (&TestClass::timer, this, 1, 1);
    timerThread.detach();
    std::thread timerThread2 (&TestClass::timer, this, 1, 2);
    timerThread2.detach();

    sleep(6);//wait for threads to finish

}

void TestClass::timer(unsigned int x, int n)//, TestClass* parent)//in seconds
{
    for(int i = 0; i < 5; i++)
    {
        printf("Timer %d expired\n", n);
        sleep(x);
        raiseEEvent(EEVENT_TIMER_EXPIRED);
    }
}

void TestClass::nullFunction(EventData *EData)
{
    // printf("****nullFuntion!!!****\n");
    throw;
    /* does whatever you want the 'nullFunction' to do. Only gets called if there was a state/event called that wasn't initialized */
}

TestClass::~TestClass()
{
}

#endif