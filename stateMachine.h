/*
 * Author:  Dustin Haring
 * Date:    May 21, 2018
 * Comment: This file contains the declarations and definitions for the state machine class
 * 
 * Modified By: n/a
 * Date:        n/a
 * Comment:     n/a
 */

#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "SafeQueue.h"
#include <stddef.h>
#include <mutex>

using namespace std;

struct EventData
{
    //virtual ~EventData() {};
};



/**
 * The EVENT_STRUCT is a basic event structure. It
 * contains constructors to handle building the event
 * quickly and works by storing the event name (as a uint) 
 * and any arguments in an EventData struct.  */
struct EVENT_STRUCT
{
    EVENT_STRUCT(unsigned int Event) { event = Event; } //constructor 1

    EVENT_STRUCT(unsigned int Event, EventData *EData)
    { //constructor 2
        event = Event;
        eData = EData;
    }

    unsigned int event; //stores the event
    EventData *eData;
};





template <class functType, class ClassType>
class StateMachine
{
    /** stateEventStruct contains a function pointer of templated type
     * functType and a uint that represents what the new state will be.
     * This struct is used in the stateIEventMap variable to store the 
     * function pointer and new state values. */
    struct stateEventStruct
    {
        stateEventStruct() {}
        stateEventStruct(functType Funct) { funct = Funct; }
        stateEventStruct(unsigned int newState, functType Funct)
        {
            NewState = newState;
            funct = Funct;
        }
        unsigned int NewState = 0;
        functType funct = 0;
    };

  public:
    StateMachine(ClassType *ClassInstance, unsigned int Max_States, unsigned int Max_IEvents, unsigned int Max_EEvents, functType nullFunct = 0);
    ~StateMachine();

    virtual void initializeStateMachineMap() = 0; //implement this in the class that inherits this class. Put all the map function calls here and call it from your constructor
    void map(unsigned int state, unsigned int IEvent, unsigned int newState, functType Funct);
    void map(unsigned int state, unsigned int IEvent, functType Funct);
    void map(unsigned int EEvent, functType Funct);

    void raiseIEvent(unsigned int IEvent, EventData *eData = NULL);
    void raiseEEvent(unsigned int EEvent);

  private:
    void StateEngine();//calls appropiate functions based on state and ievent

    mutex accessControl; //used for thread safe access calls.

    stateEventStruct **stateIEventMap; //map of from some state to another state based on internal event
    functType *EEventFunctMap;        //map of what function to call based on external event
    const unsigned int _maxStates;    //max number of states
    const unsigned int _maxIEvents;   //max number of internal events
    const unsigned int _maxEEvents;   //max number of external events

    ClassType *_classInstance;             //instance of our derived class. Used to call template functions
    unsigned int _currentState = 0;       //what state we are in currently
    SAFE_QUEUE<EVENT_STRUCT> EVENT_QUEUE; //thread safe queue containing ievent data
};

#endif